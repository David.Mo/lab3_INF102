package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int min = number.getLowerbound();
        int max = number.getUpperbound();
        while (min <= max) {
            int guess = min + (max - min) / 2;
            int result = number.guess(guess);
            if (result == 0) {
                return guess;
            } else if (result == -1) {
                min = guess + 1;
            } else {
                max = guess - 1;
            }
        }
        return 0;
    }

}
