package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int peak = 0;
        if (numbers.size() > 0) {
            peak = numbers.get(0);
            numbers.remove(0);
            peak = Math.max(peak, peakElement(numbers));
            return peak;
        }
        return 0;
    }

}
